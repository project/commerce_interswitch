7.x-4.1
---------
- Payment transactions updated on page load and via cron

7.x-3.4
---------
- Release on Drupal.org

7.x-3.3
---------
- Added filters to default view

7.x-3.2
---------
- Altered default commerce_payment view to add txn ID and requery link

7.x-3.1
---------
- Added views integration

7.x-2.0
---------
- New feature: Interswitch charge

7.x-1.1
---------
- Checkout pane removed.